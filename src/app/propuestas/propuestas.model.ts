export class Propuestas {
  title: string;
  description: string;
  votes: number;

    constructor(title: string,
      description: string,
      votes?: number) {
        this.title = title;
        this.description = description;
        this.votes = votes || 0;
    }

    voteUp() {
      this.votes++;
    }

    voteDown() {
      this.votes--;
    }
}
