import { Component, OnInit, Input, HostBinding } from '@angular/core';

import { Propuestas } from './propuestas.model';

@Component({
  selector: 'app-propuestas',
  templateUrl: './propuestas.component.html',
  styleUrls: ['./propuestas.component.css']
})
export class PropuestasComponent implements OnInit {

  @HostBinding('attr.class') cssclass="row"
  @Input() propuestas: Propuestas;

  constructor() {

  }

  ngOnInit(): void {

  }

}
