import { Component } from '@angular/core';

import { Propuestas } from './propuestas/propuestas.model';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {

  propuestas: Propuestas[];

  constructor() {
    this.propuestas = [];
  }

  addLink(title: HTMLInputElement, description: HTMLInputElement) {
    this.propuestas.push(new Propuestas(title.value, description.value));
    title.value = '';
    description.value = '';
    return false;
  }
}
